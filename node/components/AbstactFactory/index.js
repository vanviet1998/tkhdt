const { topicgetAll } = require('../topics/topic.service')
const { challengegetAll } = require('../challenge/challenge.service')

function topicFactory() {
    this.create = function () {
        return new topicgetAll()
    }
}
function challengeFactory() {
    this.create = function () {
        return new challengegetAll()
    }
}
module.exports = { topicFactory, challengeFactory }