const challenge = require("../challenge/challengeClass");
const avatar = require("../avatars/avatarClass");
const topic = require("../topics/topicClass");

class Factory {
  constructor(options) {
    if (options.class === "challenge") return new challenge(options);
    else if (options.class === "avatar") return new avatar(options);
    else if (options.class === "topic") return new topic(options);
  return
  }
}
module.exports = Factory;
