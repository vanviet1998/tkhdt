const { db, error } = require("../../helper");
const { Challenge } = db;
const data = require("../../assets/challenge.json");
const Factory = require("../factoryClass");
function getAll() {
   this.query = Challenge.find({});
   this.query.select("question level");
   this.result = this.query.exec();
}


const getById = async (id) => {
   const challenge = await Challenge.findById(id);
   const $challenge = new Factory({
      class: 'challenge',
      id: challenge.id,
      level: challenge.levl,
      choice_1: challenge.choice_1,
      levechoice_1_voicel: challenge.choice_1_voice,
      choice_2: challenge.choice_2,
      choice_2_voice: challenge.choice_2_voice,
      answer: challenge.answer,
      question: challenge.question,
      explanation: challenge.explanation,
      image: challenge.image,
   })
   return $challenge
};
module.exports = { challengegetAll: getAll, getById };
// exports.importData = async () => {
//     await Promise.all(Challenge.deleteMany())
//     await Challenge.insertMany(data)
// }
