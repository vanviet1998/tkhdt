const mongoose = require('mongoose');
const base = require('../../helper/_base_schema')

const RoomSchema = new mongoose.Schema({
    ...base,
    code: {
        type: String,
        maxlength: 2
    },
    topic: {
        type: mongoose.Schema.ObjectId,
        ref: 'Topic'
    },
    status: { // 0 : close , 1 :waiting , 2 : play
        type: Number,
        default: 1
    },
    factorX: {
        type: Number,
        default: 1
    },
    users: [{
        // -- user_id --
        // user: ObjectId
        // bot: negative number
        user_id: mongoose.SchemaTypes.Mixed,
        socket_id: String,
        // -- username --
        // user: undefined
        // bot: string
        username: String,
        not_answer: Number,
        good_answer: Number,
        false_answer: Number,
        score: {
            type: Number,
            default: 0
        },
        rank: {
            type: Number,
            default: 0
        },
        credit: {
            type: Number,
            default: 0
        },
        join_at: {
            type: Date,
            default: Date.now
        },
        disconnected_at: Date,
        isUserExited: { // if user is real player when the user exited in game
            type: Boolean,
            default: false
        },
        avatar: String
    }],
    questions: [{
        id: String, // apple music track id
        start_at: Date,
        answers: [{
            voca_id: String,
        }],
        answer_id: Number,
        user_answers: [{
            user_id: mongoose.SchemaTypes.Mixed, // from 'users.user_id'
            answer_id: Number, // answer index
            answered_at_odd: Number
        }]
    }]

});

module.exports = mongoose.model('Room', RoomSchema);
