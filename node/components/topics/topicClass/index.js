class Topic{
    constructor(options){
        this._id=options._id
        this.avatar=options.avatar
        this.mean=options.mean
        this.title=options.title
        this.vocabularies=options.vocabularies
        this.conversations=options.conversations
        this.topic_type=options.topic_type
        this.isActive=options.isActive
        this.complete=options.complete
        this.lesson_number=options.lesson_number
        this.avatar=options.avatar
        this.video=options.video
        this.lock=options.lock
        this.vocabularies=options.vocabularies
        this.sumQuestion=options.sumQuestion
        this.sumVocabulary=options.sumVocabulary
    }
}


module.exports = Topic