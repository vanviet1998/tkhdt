const topicService = require('./topic.service')
const avatarCtrl = require('../avatars/avatar.controller')
const { topicFactory } = require('../AbstactFactory')
const videoCtrl = require("../videos/video.controller");
exports.getDetails = (req, res, next) => {
    const { isGuest } = req.user;
    topicService
        .getDetails(req.params.id, isGuest)
        .then(response => {
            res.json({
                ...response,
                vocabularies: response.vocabularies.map(item => ({ ...item, avatar: avatarCtrl.getImgUrl(item.avatar) })),
                sumVocabulary: response.vocabularies.length
            })
        })
        .catch(e => next(e))
}

exports.getAll = (req, res, next) => {
    const topicfactory = new topicFactory()
    topicfactory.create().result
        .then(response => {
            const data = response.map(topic => {
                return {
                    ...topic,
                    avatar: avatarCtrl.getImgUrl(topic.avatar),
                    video: videoCtrl.getVideo(),
                    lock: !(1 === parseInt(topic.lesson_number))
                }
            })
            res.json(
                data
            )
        })
        .catch(e => next(e))
}

exports.leanTopic = (req, res, next) => {
    const { sumQuestion, _id, numberAnswer, isGuest, userForWeb } = req.user
    const payload = {
        id: req.params.id,
        sumQuestion: sumQuestion || 12,
        _id,
        isGuest: isGuest || false,
        numberAnswer,
        userForWeb
    }
    topicService
        .makeQuestion(payload)
        .then(response => res.json(response))
        .catch(e => next(e))
}