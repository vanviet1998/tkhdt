const { db, error } = require("../../helper");
const question = require("../../helper/question");
const { Topic, User } = db;
const historyService = require("../histories/history.service");
const avatarCtrl = require("../avatars/avatar.controller");
const videoCtrl = require("../videos/video.controller");
const factory = require("../../components/factoryClass");

const create = body => Topic.create(body);

function getAll() {
  this.query = Topic.find({})
  this.query.select("-vocabularies").lean();
  this.result = this.query.exec();
}

const getById = id => Topic.findById(id).populate("vocabularies");

const getDetails = async (id, isGuest = false) => {
  //if (isGuest) throw error.requiredLogin;
  const topic = await getById(id);
  const $topic = new factory({
    class: "topic",
    _id: topic._id,
    avatar: topic.avatar,
    mean: topic.mean,
    title: topic.title,
    vocabularies: topic.vocabularies,
    conversations: topic.conversations,
    topic_type: topic.topic_type,
    isActive: topic.isActive,
    complete: topic.complete,
    lesson_number: topic.lesson_number,
    avatar: avatarCtrl.getImgUrl(topic.avatar),
    video: videoCtrl.getVideo(),
    sumQuestion: topic.sumQuestion,
    sumVocabulary: topic.sumVocabulary
  });
  const topicstring = JSON.stringify($topic);

  const topicJson = JSON.parse(`${topicstring}`);

  return topicJson;
};

//todo
const makeQuestion = async ({
  id,
  sumQuestion,
  _id,
  numberAnswer,
  isGuest,
  userForWeb
}) => {
  //if (isGuest) throw error.requiredLogin;

  let vocabularies = [];
  const { histories } = await User.findById(_id).populate({
    path: "histories",
    populate: { path: "topic", populate: "vocabularies" }
  });
  const history = histories.find(
    history => history.topic._id.toString() === id
  );
  if (history) {
    const { topic, answers } = history;
    vocabularies = topic.vocabularies;
    answers.forEach(item => {
      const index = vocabularies.findIndex(
        voca => voca._id.toString() === item._id.toString() && item.correct
      );
      index >= 0 && vocabularies.splice(index, 1);
    });
  } else {
    const topics = await getById(id);
    if (!topics) throw error.topicNotFound;
    vocabularies = topics.vocabularies;
  }
  return question._makeQuestion(
    { type: "topic", numberQuestion: sumQuestion, numberAnswer, userForWeb },
    vocabularies
  );
};

const randomTopic = async () => {
  const topics = await topic.find().lean();
  const topic = topics[Math.floor(Math.random() * topics.length)];
  return {
    ...topics,
    avatar: avatarCtrl.getImgUrl(topic.avatar)
  };
};
module.exports = { topicgetAll: getAll, getById, getDetails, randomTopic, makeQuestion, create };
