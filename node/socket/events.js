// module.exports = {
//     CONNECTION: 'connection',
//     DISCONNECT: 'disconnect',
//     INVITE_FRIEND: 'INVITE_FRIEND',
//     QUIZ_JOIN: 'quiz-join',
//     QUIZ_ROOM: 'quiz-room',
//     QUIZ_START_LOADING_PLAYERS: 'quiz-start-loading-players',
//     QUIZ_UPDATE_PLAYERS: 'quiz-update-players',
//     QUIZ_JOIN: 'quiz-join',
//     QUIZ_LEAVE: 'quiz-leave',
//     QUIZ_QUEUE_USER: 'quiz-queue-user',
//     QUIZ_JOIN_SUCCESS: 'quiz-join-success',
//     QUIZ_JOIN_ERROR: 'quiz-join-error',
//     QUIZ_NEW_PLAYER: 'quiz-new-user',
//     QUIZ_END_LOADING_PLAYERS: 'quiz-end-loading-players',
//     QUIZ_START_COUNTDOWN: 'quiz-start-countdown',
//     QUIZ_WAIT_ANIMATION: 'quiz-wait-animation',

//     QUIZ_JOIN_SUDDEN_DEATH_SUCCESS: 'quiz-join-sudden-death-success',
//     QUIZ_LEAVE_SUDDEN_DEATH_SUCCESS: 'quiz-leave-sudden-death-success',
//     QUIZ_START_VOTING_THEME: 'quiz-start-voting-theme',
//     QUIZ_END_VOTING_THEME: 'quiz-end-voting-theme',
//     QUIZ_VOTE_THEME: 'quiz-vote-theme',
//     QUIZ_CHANGE_VOTING_THEMES: 'quiz-change-voting-themes',

//     QUIZ_START_QUESTION: 'quiz-question',
//     QUIZ_ANSWER: 'quiz-answer',
//     QUIZ_ANSWER_RESULT: 'quiz-answer-result',
//     QUIZ_PREPARE_QUESTION: 'quiz-prepare-question',
//     QUIZ_CLOSED: 'quiz-closed',
//     QUIZ_LINK_MUSICS: 'quiz-link-musics',
//     QUIZ_DELAY_LINK_MUSICS: 'quiz-delay-link-music',
//     QUIZ_LINK_SUDDEN_DEATH: 'quiz-link-sudden-death',
//     CREATE_PRIVATE_ROOM: 'create-private-room',
//     START_GAME_IN_PRIVATE_ROOM: 'start-game-in-private-room',
//     NOT_ENOUGH_CRITERIA: 'not-enough-criteria',
//     RETURN_THEME_FOR_ROOM: 'return-theme-for-room',
//     NEW_PLAYER: 'new-player',
//     UPDATE_LIST_USER: 'update-list-user',
//     CHOOSE_THEME: 'choose-theme',
//     CHOOSE_MODE: 'choose-mode',
//     CHOOSE_NUMBER_SONG: 'choose-number-song',
//     RETURN_THEME_EXCEPT_SENDER: 'return-theme-except-sender',
//     RETURN_MODE_EXCEPT_SENDER: 'return-mode-except-sender',
//     RETURN_SONG_EXCEPT_SENDER: 'return-song-except-sender',
//     LEAVE_ROOM: 'leave-room',
//     ROOM_NOT_EXIST: 'room-not-exist',
//     QUIZ_ROOM: 'quiz-room',
//     GET_CONFIG_ROOM: 'get-config-room',
//     RETURN_CONFIG_ROOM: 'return-config-room',
//     RETURN_ROOM_ID: 'return-room-id',
//     QUIZ_ANSWER_PRIVATE: 'quiz-answer-private',
//     ROOM_FULL: 'room-full',
//     UPDATE_ROOM_USERS: 'update-room-users',
//     CHANGE_NAME_OR_AVATAR: 'change-name-or-avatar',
//     RETURN_CONFIG_ROOM: 'return-config-room',
//     RETURN_THEMES_FOR_ROOM: 'return-themes-for-room',
//     RETURN_MODE_FOR_ROOM: 'return-mode-for-room',
//     RETURN_NUMBER_SONG_FOR_ROOM: 'return-number-song-for-room',
//     UPDATE_CONFIG_ROOM: 'update-config-room'

// }