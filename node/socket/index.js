// const socketIO = require('socket.io');
// const redisCli = require('redis');
// const redisAdapter = require('socket.io-redis');
// const userService = require('../components/users/user.service')
// const roomService = require('../components/rooms/room.service')
// const EVENTS = require('./events');
// const client = redisCli.createClient({
//     host: process.env.REDIS_HOST,
//     port: process.env.REDIS_PORT,
// });

// exports.socket = null;

// exports.createServer = ({ server }) => {
//     const io = socketIO(server);
//     io.adapter(redisAdapter({
//         host: process.env.REDIS_HOST,
//         port: process.env.REDIS_PORT,
//     }));
//     this.socket = io;
//     const socketRoom = (room_id) => io.to(room_id);
//     io.on(EVENTS.CONNECTION, socket => {

//         const userId = socket.handshake.query.id || socket.handshake.query.user_id;

//         userService.findByIdAndUpdate({ _id: userId, isOnline: true, socket_id: socket.id }).then(res => res)

//         socket.on(EVENTS.QUIZ_JOIN, (data) => quizJoin({ ...data, socketId: socket.id, socketRoom, socket }))

//         console.log(`userId: ${userId} -> socketId: ${socket.id}`)
//         socket.on(EVENTS.DISCONNECT, reason => {
//             userService.findByIdAndUpdate({ _id: userId, isOnline: false, socketId: '' }).then(res => res)
//         })
//     })
//     return io;
// }

// const quizJoin = async (data) => {
//     const { room_id, user_id, socket_id, socketRoom, socket } = data;
//     const room = await roomService.join(data)
//     const user = room.users.find((user) => (
//         (user.user_id === data.user_id) &&
//         (user.socket_id === socket.id)
//     ));
//     // NOT REJOIN
//     // if (!data.room_id) {
//     //     socketRoom(room_id).emit(EVENTS.QUIZ_NEW_PLAYER, user);
//     //     socket.emit(EVENTS.QUIZ_JOIN_SUCCESS, room);
//     //     socket.join(room_id);
//     //     roomService.startRoom({ room, socketRoom, EVENTS });
//     // } else {
//     //     socket.join(room_id);
//     // }

//     // socketRoom(room_id).emit(EVENTS.QUIZ_ROOM, room);

//     // socket.on(EVENTS.QUIZ_ANSWER, ({ question_id, answer_id, user_id }) => {
//     //     roomService.answerQuestion({ room_id, question_id, answer_id, user_id }).then((result) => {
//     //         result && socketRoom(room_id).emit(EVENTS.QUIZ_ANSWER_RESULT, result);
//     //     });
//     // });

// }